from flask import Flask, flash, request, redirect, url_for, render_template, session
from function import data, addData, delDevice, specificList, editDevice
import yaml
import os

app = Flask(__name__)
app.secret_key = 'any random'


@app.route('/add', methods=['GET', 'POST'])
def add():
    if request.method == 'POST':
        controller_data = request.form['controller_data']
        device_code = request.form['device_code']
        name0 = request.form['name']
        platform0 = request.form['platform']
        unique_id = request.form['unique_id']
        fan_mode_command_topic = request.form['fan_mode_command_topic']
        fan_mode_state_topic = request.form['fan_mode_state_topic']
        max_temp = request.form['max_temp']
        min_temp = request.form['min_temp']
        mode_command_topic = request.form['mode_command_topic']
        mode_state_topic = request.form['mode_state_topic']
        name = request.form['name']
        platform = request.form['platform']
        power_command_topic = request.form['power_command_topic']
        precision = request.form['precision']
        swing_mode_command_topic = request.form['swing_mode_command_topic']
        swing_mode_state_topic = request.form['swing_mode_state_topic']
        temperature_command_topic = request.form['temperature_command_topic']
        temperature_state_topic = request.form['temperature_state_topic']
        addData(controller_data, device_code, name0, platform0, unique_id, fan_mode_command_topic, fan_mode_state_topic, max_temp, min_temp, mode_command_topic,
                mode_state_topic, name, platform, power_command_topic, precision, swing_mode_command_topic, swing_mode_state_topic, temperature_command_topic, temperature_state_topic)
        return render_template("home.html", list=data())
    else:
        return render_template("add.html")


@app.route('/edit', methods=['GET', 'POST'])
def edit():
    if request.method == 'POST':
        # position = request.form.get('edit')
        position = session['position']
        print("----------------------------------------")
        print(">>>>>>>>>>>>>>>>>>>>>>>>>", position)
        print("----------------------------------------")
        controller_data = request.form['controller_data']
        device_code = request.form['device_code']
        name0 = request.form['name']
        platform0 = request.form['platform']
        unique_id = request.form['unique_id']
        fan_mode_command_topic = request.form['fan_mode_command_topic']
        fan_mode_state_topic = request.form['fan_mode_state_topic']
        max_temp = request.form['max_temp']
        min_temp = request.form['min_temp']
        mode_command_topic = request.form['mode_command_topic']
        mode_state_topic = request.form['mode_state_topic']
        name = request.form['name']
        platform = request.form['platform']
        power_command_topic = request.form['power_command_topic']
        precision = request.form['precision']
        swing_mode_command_topic = request.form['swing_mode_command_topic']
        swing_mode_state_topic = request.form['swing_mode_state_topic']
        temperature_command_topic = request.form['temperature_command_topic']
        temperature_state_topic = request.form['temperature_state_topic']
        editDevice(position, controller_data, device_code, name0, platform0, unique_id, fan_mode_command_topic, fan_mode_state_topic, max_temp, min_temp, mode_command_topic,
                   mode_state_topic, name, platform, power_command_topic, precision, swing_mode_command_topic, swing_mode_state_topic, temperature_command_topic, temperature_state_topic)
        return render_template('home.html', list=data())


@app.route('/renderEdit', methods=['GET', 'POST'])
def renderEdit():
    position = request.form.get('edit')
    session['position'] = position
    print("----------------------------------------")
    print(position, "<<<<<<<<<<<<<<<<<<<<<<<<<")
    print("----------------------------------------")
    list = specificList(position)
    print(list)
    controller_data = list[1]
    device_code = list[2]
    name0 = list[3]
    platform0 = list[4]
    unique_id = list[5]
    fan_mode_command_topic = list[6]
    fan_mode_state_topic = list[7]
    max_temp = list[8]
    min_temp = list[9]
    mode_command_topic = list[10]
    mode_state_topic = list[11]
    name = list[12]
    platform = list[13]
    power_command_topic = list[14]
    precision = list[15]
    swing_mode_command_topic = list[16]
    swing_mode_state_topic = list[17]
    temperature_command_topic = list[18]
    temperature_state_topic = list[19]
    return render_template('edit.html', position=position, controller_data=controller_data, device_code=device_code, name0=name0, platform0=platform0,
                           fan_mode_state_topic=fan_mode_state_topic, fan_mode_command_topic=fan_mode_command_topic,
                           max_temp=max_temp, min_temp=min_temp, mode_command_topic=mode_command_topic, mode_state_topic=mode_state_topic,
                           name=name, platform=platform, power_command_topic=power_command_topic, precision=precision, swing_mode_command_topic=swing_mode_command_topic,
                           swing_mode_state_topic=swing_mode_state_topic, temperature_command_topic=temperature_command_topic, temperature_state_topic=temperature_state_topic)


@app.route('/', methods=['GET', 'POST'])
def home():
    if request.method == 'POST':
        position = request.form.get('delete')
        delDevice(position)
    return render_template("home.html", list=data())


if __name__ == '__main__':
    app.run(debug="true")
