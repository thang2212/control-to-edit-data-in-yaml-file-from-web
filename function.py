import yaml
import os

FOLDER = r"C:\Users\bacht\Desktop\intern\180520"


def loadData():
    # loadData method returns data in file data.yaml
    file = os.path.join(FOLDER, 'data.yaml')
    with open(file, 'r', encoding="utf8") as file:
        # The FullLoader parameter handles the conversion from YAML
        # scalar values to Python the dictionary format
        data = yaml.load(file, Loader=yaml.FullLoader)
        # print(data)
    return data


# print(loadData())


def data():
    data = loadData()
    list = []
    position = 0
    i = 0
    for dict in data:
        if i % 2 == 0:
            arrDevice = []
            arrDevice.append(position)
            position += 1
            arrDevice.append(dict['controller_data'])
            arrDevice.append(dict['device_code'])
            arrDevice.append(dict['name'])
            arrDevice.append(dict['platform'])
            arrDevice.append(dict['unique_id'])
            i += 1
            # print(arrDevice)
        else:
            arrDevice.append(dict['fan_mode_command_topic'])
            arrDevice.append(dict['fan_mode_state_topic'])
            arrDevice.append(dict['fan_modes'])
            arrDevice.append(dict['max_temp'])
            arrDevice.append(dict['min_temp'])
            arrDevice.append(dict['mode_command_topic'])
            arrDevice.append(dict['mode_state_topic'])
            arrDevice.append(dict['modes'])
            arrDevice.append(dict['name'])
            arrDevice.append(dict['platform'])
            arrDevice.append(dict['power_command_topic'])
            arrDevice.append(dict['precision'])
            arrDevice.append(dict['swing_mode_command_topic'])
            arrDevice.append(dict['swing_mode_state_topic'])
            arrDevice.append(dict['temperature_command_topic'])
            arrDevice.append(dict['temperature_state_topic'])
            list.append(arrDevice)
            i += 1
    # print(list)
    return list


# data()
# print(data())
# print(len(data()))


def specificList(position):
    # print("specificList()")
    dicts = loadData()
    list = []
    i = 0
    k = 0
    arrDevice = []
    for dict in dicts:
        if k % 2 == 0:
            arrDevice.append(i)  # order no.
            arrDevice.append(dict['controller_data'])
            arrDevice.append(dict['device_code'])
            arrDevice.append(dict['name'])
            arrDevice.append(dict['platform'])
            arrDevice.append(dict['unique_id'])
            k += 1
        else:
            arrDevice.append(dict['fan_mode_command_topic'])
            arrDevice.append(dict['fan_mode_state_topic'])
            arrDevice.append(dict['max_temp'])
            arrDevice.append(dict['min_temp'])
            arrDevice.append(dict['mode_command_topic'])
            arrDevice.append(dict['mode_state_topic'])
            arrDevice.append(dict['name'])
            arrDevice.append(dict['platform'])
            arrDevice.append(dict['power_command_topic'])
            arrDevice.append(dict['precision'])
            arrDevice.append(dict['swing_mode_command_topic'])
            arrDevice.append(dict['swing_mode_state_topic'])
            arrDevice.append(dict['temperature_command_topic'])
            arrDevice.append(dict['temperature_state_topic'])
            k += 1
            list.append(arrDevice)
            arrDevice = []
        i += 1
    return list[int(position)]


# print(specificList(0))


def addData(controller_data, device_code, name0, platform0, unique_id, fan_mode_command_topic, fan_mode_state_topic, max_temp, min_temp, mode_command_topic, mode_state_topic, name, platform, power_command_topic, precision, swing_mode_command_topic, swing_mode_state_topic, temperature_command_topic, temperature_state_topic):
    dicts = loadData()
    # print("addData()")
    device = {}

    if controller_data != None:
        device['controller_data'] = controller_data
    if device_code != None:
        device['device_code'] = device_code
    if name0 != None:
        device['name'] = name0
    if platform0 != None:
        device['platform'] = platform0
    if unique_id != None:
        device['unique_id'] = unique_id
    dicts.append(device)
    device = {}
    if fan_mode_command_topic != None:
        device['fan_mode_command_topic'] = fan_mode_command_topic
    if fan_mode_state_topic != None:
        device['fan_mode_state_topic'] = fan_mode_state_topic
    device['fan_modes'] = ['auto', 'high',
                           'medium', 'low', 'med', 'max', 'min']
    if max_temp != None:
        device['max_temp'] = max_temp
    if min_temp != None:
        device['min_temp'] = min_temp
    if mode_command_topic != None:
        device['mode_command_topic'] = mode_command_topic
    if mode_state_topic != None:
        device['mode_state_topic'] = mode_state_topic
    device['modes'] = ['off', 'cool', 'dry', 'fan_only', 'heat', 'auto']
    if name != None:
        device['name'] = name
    if platform != None:
        device['platform'] = platform
    if power_command_topic != None:
        device['power_command_topic'] = power_command_topic
    if precision != None:
        device['precision'] = precision
    if swing_mode_command_topic != None:
        device['swing_mode_command_topic'] = swing_mode_command_topic
    if swing_mode_state_topic != None:
        device['swing_mode_state_topic'] = swing_mode_state_topic
    if temperature_command_topic != None:
        device['temperature_command_topic'] = temperature_command_topic
    if temperature_state_topic != None:
        device['temperature_state_topic'] = temperature_state_topic
    dicts.append(device)
    file = os.path.join(FOLDER, 'data.yaml')
    with open(file, "w") as f:
        yaml.dump(dicts, f, default_flow_style=False)


def delDevice(position):
    dicts = loadData()
    # print("delDevice()")
    del dicts[int(position)+1]
    del dicts[int(position)+1]
    file = os.path.join(FOLDER, 'data.yaml')
    with open(file, "w") as f:
        yaml.dump(dicts, f, default_flow_style=False)


# delDevice(5)


def editDevice(position, controller_data, device_code, name0, platform0, unique_id, fan_mode_command_topic, fan_mode_state_topic, max_temp, min_temp, mode_command_topic, mode_state_topic, name, platform, power_command_topic, precision, swing_mode_command_topic, swing_mode_state_topic, temperature_command_topic, temperature_state_topic):
    dicts = loadData()
    dict = dicts[int(position)]
    if dict['controller_data'] != None:
        dict['controller_data'] = controller_data
    if dict['device_code'] != None:
        dict['device_code'] = device_code
    if dict['name'] != None:
        dict['name'] = name0
    if dict['platform'] != None:
        dict['platform'] = platform0
    if dict['unique_id'] != None:
        dict['unique_id'] = unique_id
    dict = dicts[int(position)+1]
    if dict['fan_mode_command_topic'] != None:
        dict['fan_mode_command_topic'] = fan_mode_command_topic
    if dict['fan_mode_state_topic'] != None:
        dict['fan_mode_state_topic'] = fan_mode_state_topic
    dict['fan_modes'] = ['off', 'cool', 'dry', 'fan_only', 'heat', 'auto']
    if dict['max_temp'] != None:
        dict['max_temp'] = max_temp
    if dict['min_temp'] != None:
        dict['min_temp'] = min_temp
    if dict['mode_command_topic'] != None:
        dict['mode_command_topic'] = mode_command_topic
    if dict['mode_state_topic'] != None:
        dict['mode_state_topic'] = mode_state_topic
    dict['modes'] = ['off', 'cool', 'dry', 'fan_only', 'heat', 'auto']
    if dict['name'] != None:
        dict['name'] = name
    if dict['platform'] != None:
        dict['platform'] = platform
    if dict['power_command_topic'] != None:
        dict['power_command_topic'] = power_command_topic
    if dict['precision'] != None:
        dict['precision'] = precision
    if dict['swing_mode_command_topic'] != None:
        dict['swing_mode_command_topic'] = swing_mode_command_topic
    if dict['swing_mode_state_topic'] != None:
        dict['swing_mode_state_topic'] = swing_mode_state_topic
    if dict['temperature_command_topic'] != None:
        dict['temperature_command_topic'] = temperature_command_topic
    if dict['temperature_state_topic'] != None:
        dict['temperature_state_topic'] = temperature_state_topic

    file = os.path.join(FOLDER, 'data.yaml')
    with open(file, "w") as f:
        yaml.dump(dicts, f, default_flow_style=False)


# editDevice(5, 'a', 'h', 'e')
